FactoryGirl.define do
  factory :user do
    name "User"
    password  "123456teste"
    sequence(:email) { |n| "#{n}@test.com"}
    password_confirmation "123456teste"
    factory :admin_user do
      role "admin"
    end
    factory :regular_user do
      role "regular"
    end
    factory :user_manager_user do
      role "user_manager"
    end
  end
end
