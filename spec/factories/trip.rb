FactoryGirl.define do
  factory :trip do
    destination "Destination"
    start_date Date.today
    end_date Date.today + 20.day
    comment "Comment"
    association :user, factory: :regular_user
  end
end
