require 'rails_helper'

RSpec.describe Trip, type: :model do
  let(:trip) { FactoryGirl.build(:trip) }
  subject { trip }
  it { should validate_presence_of :destination }
  it { should validate_presence_of :start_date }
  it { should validate_presence_of :end_date }

  describe '#missing_days?' do
    it 'should return missing days if in the future' do
      @trip = FactoryGirl.create(:trip, start_date: Date.today + 5.day)
      expect(@trip.missing_days).to eq(5)
    end
    it 'should return -1 if in the past' do
      @trip = FactoryGirl.create(:trip, start_date: Date.today - 5.day)
      expect(@trip.missing_days).to eq(-1)
    end
  end

  describe '#end_date_bigger_than_start_date?' do
    it 'should be valid' do
      @trip = FactoryGirl.build(:trip, start_date: Date.today, end_date: Date.tomorrow)
      expect(@trip.valid?).to be true
    end
    it 'should not be valid' do
      @trip = FactoryGirl.build(:trip, start_date: Date.today, end_date: Date.yesterday)
      expect(@trip.valid?).to be false
    end
  end

end
