require 'rails_helper'

RSpec.describe User, type: :model do
  let(:user) { FactoryGirl.build(:user) }
  subject { user }
  it { should validate_presence_of :name }
  it { should validate_presence_of :password }
  it { should validate_presence_of :email }

  describe '#regular?' do
    it 'should return true if user is regular' do
      @user = FactoryGirl.create(:regular_user)
      expect(@user.regular?).to be true
    end
    it 'should return false if user is not regular' do
      @user = FactoryGirl.create(:admin_user)
      expect(@user.regular?).to be false
    end
  end

  describe '#admin?' do
    it 'should return true if user is admin' do
      @user = FactoryGirl.create(:admin_user)
      expect(@user.admin?).to be true
    end
    it 'should return false if user is not admin' do
      @user = FactoryGirl.create(:regular_user)
      expect(@user.admin?).to be false
    end
  end

  describe '#user_manager?' do
    it 'should return true if user is user_manager' do
      @user = FactoryGirl.create(:user_manager_user)
      expect(@user.user_manager?).to be true
    end
    it 'should return false if user is not user_manager' do
      @user = FactoryGirl.create(:admin_user)
      expect(@user.user_manager?).to be false
    end
  end
end
