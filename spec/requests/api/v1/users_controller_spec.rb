describe "Users API" do
  let(:admin_user) { FactoryGirl.create(:admin_user) }
  let(:regular_user) { FactoryGirl.create(:regular_user) }
  let(:user_manager_user) { FactoryGirl.create(:user_manager_user) }

  context 'Get' do
    it 'all users as admin' do
      get '/api/v1/users/', nil, admin_user.create_new_auth_token
      json = JSON.parse(response.body)
      expect(response).to be_success
      expect(json.length).to eq(User.all.size)
    end
    it 'all users as user manager' do
      get '/api/v1/users/', nil, user_manager_user.create_new_auth_token
      json = JSON.parse(response.body)
      expect(response).to be_success
      expect(json.length).to eq(User.all.size)
    end
    it 'all users as regular user' do
      get '/api/v1/users/', nil, regular_user.create_new_auth_token
      expect(response).to be_unauthorized
    end
    it 'my user as admin' do
      get '/api/v1/users/' + admin_user.id.to_s, nil, admin_user.create_new_auth_token
      expect(response).to be_success
    end
    it 'my user as user manager' do
      get '/api/v1/users/' + user_manager_user.id.to_s, nil, user_manager_user.create_new_auth_token
      expect(response).to be_success
    end
    it 'my user as regular user' do
      get '/api/v1/users/' + regular_user.id.to_s, nil, regular_user.create_new_auth_token
      expect(response).to be_success
    end
  end

  context 'Create User' do
    it 'with valid parameters' do
      user = FactoryGirl.build(:user)
      post('/api/v1/auth/',
           {
             name: user.name,
             email: user.email,
             password: user.password,
             password_confirmation: user.password_confirmation,
           })
      expect(response).to be_success
    end
    it 'with invalid parameters' do
      user = FactoryGirl.build(:user)
      post('/api/v1/auth/',
           {
             name: user.name,
             email: user.email,
             password: user.password,
             password_confirmation: user.password_confirmation + '1',
           })
      expect(response).to have_http_status(422)
    end
  end

  context 'Update User' do
    it 'with valid parameters' do
      user = FactoryGirl.create(:user)
      patch('/api/v1/users/' + user.id.to_s,
            {user: {
              name: user.name,
              email: user.email,
              role: user.role
            }},
           admin_user.create_new_auth_token)
      expect(response).to be_success
    end
    it 'with missing parameters' do
      user = FactoryGirl.create(:user)
      patch('/api/v1/users/' + user.id.to_s,
           {user: {
             name: '',
             email: '',
             role: ''
           }},
           admin_user.create_new_auth_token)
      json = JSON.parse(response.body)
      expect(response).to have_http_status(400)
      expect(json.length).to eq(5)
    end
    it 'for another user as user_manager' do
      user = FactoryGirl.create(:user)
      patch('/api/v1/users/' + user.id.to_s,
            {user: {
              name: user.name,
              email: user.email,
              role: user.role
            }},
           user_manager_user.create_new_auth_token)
      expect(response).to be_success
    end
    it 'for another user as regular user' do
      user = FactoryGirl.create(:user)
      patch('/api/v1/users/' + user.id.to_s,
            {user: {
              name: user.name,
              email: user.email,
              role: user.role
            }},
           regular_user.create_new_auth_token)
      expect(response).to be_unauthorized
    end
    it 'for same user as regular user' do
      user = regular_user
      patch('/api/v1/users/' + user.id.to_s,
            {user: {
              name: user.name,
              email: user.email,
              role: user.role
            }},
           regular_user.create_new_auth_token)
      expect(response).to be_success
    end
    it 'with invalid id' do
      user = FactoryGirl.create(:user)
      user.delete
      patch('/api/v1/users/' + user.id.to_s,
            {user: {
              name: user.name,
              email: user.email,
              role: user.role
            }},
           regular_user.create_new_auth_token)
      expect(response).to have_http_status(404)
    end
  end

  context 'Delete User' do
    it 'with valid id' do
      user = FactoryGirl.create(:user)
      delete('/api/v1/users/' + user.id.to_s, admin_user.create_new_auth_token)
      expect(response).to be_success
    end
    it 'with invalid id' do
      user = FactoryGirl.create(:user)
      user.delete
      delete('/api/v1/users/' + user.id.to_s, admin_user.create_new_auth_token)
      expect(response).to have_http_status(404)
    end
    it 'for another user as regular user' do
      user = FactoryGirl.create(:user)
      delete('/api/v1/users/' + user.id.to_s, regular_user.create_new_auth_token)
      expect(response).to be_unauthorized
    end
    it 'for another user as admin user' do
      user = FactoryGirl.create(:user)
      delete('/api/v1/users/' + user.id.to_s, admin_user.create_new_auth_token)
      expect(response).to be_success
    end
    it 'for another user as user manager user' do
      user = FactoryGirl.create(:user)
      delete('/api/v1/users/' + user.id.to_s, user_manager_user.create_new_auth_token)
      expect(response).to be_success
    end
  end


end
