describe "Trips API" do
  let(:admin_user) { FactoryGirl.create(:admin_user) }
  let(:regular_user) { FactoryGirl.create(:regular_user) }
  let(:user_manager_user) { FactoryGirl.create(:user_manager_user) }

  context 'Get' do
    it 'all trips as admin' do
      FactoryGirl.create_list(:trip, 10)
      get '/api/v1/trips', nil, admin_user.create_new_auth_token
      json = JSON.parse(response.body)
      expect(response).to be_success
      expect(json.length).to eq(10)
    end
    it 'all trips as user manager' do
      FactoryGirl.create_list(:trip, 10)
      get '/api/v1/trips', nil, user_manager_user.create_new_auth_token
      json = JSON.parse(response.body)
      expect(response).to be_unauthorized
    end
    it 'all trips as regular user' do
      FactoryGirl.create_list(:trip, 10)
      get '/api/v1/trips', nil, regular_user.create_new_auth_token
      json = JSON.parse(response.body)
      expect(response).to be_unauthorized
    end
    it 'my trips as admin' do
      FactoryGirl.create_list(:trip, 10)
      get '/api/v1/trips?by_user_id=' + admin_user.id.to_s, nil, admin_user.create_new_auth_token
      json = JSON.parse(response.body)
      expect(response).to be_success
      expect(json.length).to eq(0)
    end
    it 'my trips as user manager' do
      FactoryGirl.create_list(:trip, 10)
      get '/api/v1/trips?by_user_id=' + user_manager_user.id.to_s, nil, user_manager_user.create_new_auth_token
      json = JSON.parse(response.body)
      expect(response).to be_success
      expect(json.length).to eq(0)
    end
    it 'my trips as regular user' do
      FactoryGirl.create_list(:trip, 10)
      get '/api/v1/trips?by_user_id=' + regular_user.id.to_s, nil, regular_user.create_new_auth_token
      json = JSON.parse(response.body)
      expect(response).to be_success
      expect(json.length).to eq(0)
    end
  end


  context 'Filter Trips' do
    it 'by comment' do
      FactoryGirl.create_list(:trip, 10, comment: 'test')
      FactoryGirl.create_list(:trip, 10, comment: 'abcd')
      get '/api/v1/trips?by_comment=abcd', nil, admin_user.create_new_auth_token
      json = JSON.parse(response.body)
      expect(response).to be_success
      expect(json.length).to eq(10)
    end
    it 'by start_date' do
      FactoryGirl.create_list(:trip, 10, start_date: Date.today)
      FactoryGirl.create_list(:trip, 10, start_date: Date.tomorrow)
      get '/api/v1/trips?by_start_date=' + Date.tomorrow.strftime('%F'), nil, admin_user.create_new_auth_token
      json = JSON.parse(response.body)
      expect(response).to be_success
      expect(json.length).to eq(10)
    end
    it 'by start_date' do
      FactoryGirl.create_list(:trip, 10, end_date: Date.today)
      FactoryGirl.create_list(:trip, 10, end_date: Date.tomorrow)
      get '/api/v1/trips?by_end_date=' + Date.today.strftime('%F'), nil, admin_user.create_new_auth_token
      json = JSON.parse(response.body)
      expect(response).to be_success
      expect(json.length).to eq(10)
    end
    it 'by start_date le and be' do
      FactoryGirl.create_list(:trip, 10, start_date: Date.today)
      FactoryGirl.create_list(:trip, 10, start_date: Date.today + 15.day)
      get '/api/v1/trips?by_start_date_le=' + (Date.today + 7.day).strftime('%F') + "&by_start_date_be=" + (Date.today - 7.day).strftime('%F'), nil, admin_user.create_new_auth_token
      json = JSON.parse(response.body)
      expect(response).to be_success
      expect(json.length).to eq(10)
    end
  end


  context 'Create Trip' do
    it 'with valid parameters' do
      trip = FactoryGirl.build(:trip)
      post('/api/v1/trips',
           {trip: {
             destination: trip.destination,
             start_date: trip.start_date.strftime('%F'),
             end_date: trip.end_date.strftime('%F'),
             comment: trip.comment,
             user_id: trip.user_id
           }},
           admin_user.create_new_auth_token)
      expect(response).to be_success
    end
    it 'with missing parameters' do
      trip = FactoryGirl.build(:trip)
      post('/api/v1/trips',
           {trip: {
             destination: trip.destination,
             user_id: trip.user_id
           }},
           admin_user.create_new_auth_token)

      json = JSON.parse(response.body)
      expect(response).to have_http_status(400)
      expect(json.length).to eq(2)
    end
    it 'for another user as user_manager' do
      trip = FactoryGirl.build(:trip)
      post('/api/v1/trips',
           {trip: {
             destination: trip.destination,
             start_date: trip.start_date.strftime('%F'),
             end_date: trip.end_date.strftime('%F'),
             comment: trip.comment,
             user_id: trip.user_id
           }},
           user_manager_user.create_new_auth_token)
      expect(response).to be_unauthorized
    end
    it 'for another user as regular user' do
      trip = FactoryGirl.build(:trip)
      post('/api/v1/trips',
           {trip: {
             destination: trip.destination,
             start_date: trip.start_date.strftime('%F'),
             end_date: trip.end_date.strftime('%F'),
             comment: trip.comment,
             user_id: trip.user_id
           }},
           regular_user.create_new_auth_token)
      expect(response).to be_unauthorized
    end
    it 'for same user as regular user' do
      trip = FactoryGirl.build(:trip, user_id: regular_user.id)
      post('/api/v1/trips',
           {trip: {
             destination: trip.destination,
             start_date: trip.start_date.strftime('%F'),
             end_date: trip.end_date.strftime('%F'),
             comment: trip.comment,
             user_id: trip.user_id
           }},
           regular_user.create_new_auth_token)
      expect(response).to be_success
    end
  end


  context 'Update Trip' do
    it 'with valid parameters' do
      trip = FactoryGirl.create(:trip)
      patch('/api/v1/trips/' + trip.id.to_s,
           {trip: {
             destination: trip.destination,
             start_date: trip.start_date.strftime('%F'),
             end_date: trip.end_date.strftime('%F'),
             comment: trip.comment,
             user_id: trip.user_id
           }},
           admin_user.create_new_auth_token)
      expect(response).to be_success
    end
    it 'with missing parameters' do
      trip = FactoryGirl.create(:trip)
      patch('/api/v1/trips/' + trip.id.to_s,
           {trip: {
             destination: trip.destination,
             start_date: '',
             end_date: '',
             comment: '',
             user_id: trip.user_id
           }},
           admin_user.create_new_auth_token)
      json = JSON.parse(response.body)
      expect(response).to have_http_status(400)
      expect(json.length).to eq(2)
    end
    it 'for another user as user_manager' do
      trip = FactoryGirl.create(:trip)
      patch('/api/v1/trips/' + trip.id.to_s,
           {trip: {
             destination: trip.destination,
             start_date: trip.start_date.strftime('%F'),
             end_date: trip.end_date.strftime('%F'),
             comment: trip.comment,
             user_id: trip.user_id
           }},
           user_manager_user.create_new_auth_token)
      expect(response).to be_unauthorized
    end
    it 'for another user as regular user' do
      trip = FactoryGirl.create(:trip)
      patch('/api/v1/trips/' + trip.id.to_s,
           {trip: {
             destination: trip.destination,
             start_date: trip.start_date.strftime('%F'),
             end_date: trip.end_date.strftime('%F'),
             comment: trip.comment,
             user_id: trip.user_id
           }},
           regular_user.create_new_auth_token)
      expect(response).to be_unauthorized
    end
    it 'for same user as regular user' do
      trip = FactoryGirl.create(:trip, user_id: regular_user.id)
      patch('/api/v1/trips/' + trip.id.to_s,
           {trip: {
             destination: trip.destination,
             start_date: trip.start_date.strftime('%F'),
             end_date: trip.end_date.strftime('%F'),
             comment: trip.comment,
             user_id: trip.user_id
           }},
           regular_user.create_new_auth_token)
      expect(response).to be_success
    end
    it 'with invalid id' do
      trip = FactoryGirl.create(:trip, user_id: regular_user.id)
      trip.delete
      patch('/api/v1/trips/' + trip.id.to_s,
           {trip: {
             destination: trip.destination,
             start_date: trip.start_date.strftime('%F'),
             end_date: trip.end_date.strftime('%F'),
             comment: trip.comment,
             user_id: trip.user_id
           }},
           regular_user.create_new_auth_token)
      expect(response).to have_http_status(404)
    end
  end

  context 'Delete Trip' do
    it 'with valid id' do
      trip = FactoryGirl.create(:trip, user_id: regular_user.id)
      delete('/api/v1/trips/' + trip.id.to_s, regular_user.create_new_auth_token)
      expect(response).to be_success
    end
    it 'with invalid id' do
      trip = FactoryGirl.create(:trip, user_id: regular_user.id)
      trip.delete
      delete('/api/v1/trips/' + trip.id.to_s, regular_user.create_new_auth_token)
      expect(response).to have_http_status(404)
    end
    it 'for another user as regular user' do
      trip = FactoryGirl.create(:trip)
      delete('/api/v1/trips/' + trip.id.to_s, regular_user.create_new_auth_token)
      expect(response).to be_unauthorized
    end
    it 'for another user as admin user' do
      trip = FactoryGirl.create(:trip)
      delete('/api/v1/trips/' + trip.id.to_s, admin_user.create_new_auth_token)
      expect(response).to be_success
    end
    it 'for another user as user manager user' do
      trip = FactoryGirl.create(:trip)
      delete('/api/v1/trips/' + trip.id.to_s, user_manager_user.create_new_auth_token)
      expect(response).to be_unauthorized
    end
  end
end
