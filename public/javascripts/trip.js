TripsApp.updateTrip = function( id, destination, startDate, endDate, comment, userId ){
  TripsApp.patchRequest("/api/v1/trips/" + id,
                       {
                         trip: {
                           destination: destination,
                           start_date: startDate ,
                           end_date: endDate,
                           comment: comment,
                           user_id: userId
                         }
                       },
                      function( data, status, request ){
                        TripsApp.lastAction();
                        $.smkAlert({
                          text: 'Successfully updated trip!',
                          type: 'success'
                        });
                      },
                      function(){});
};

TripsApp.createTrip = function( destination, startDate, endDate, comment, userId = TripsApp.user.id){
  TripsApp.postRequest("/api/v1/trips",
                       {
                         trip: {
                           destination: destination,
                           start_date: startDate ,
                           end_date: endDate,
                           comment: comment,
                           user_id: userId
                        }
                       },
                      function( data, status, request ){
                        TripsApp.lastAction();
                        $.smkAlert({
                          text: 'Successfully created trip!',
                          type: 'success'
                        });
                      },
                      function(){});
};

TripsApp.deleteTrip = function( id ){
  TripsApp.deleteRequest("/api/v1/trips/" + id,
                      function( data, status, request ){
                        TripsApp.lastAction();
                        $.smkAlert({
                          text: 'Successfully deleted trip!',
                          type: 'success'
                        });
                      },
                      function(){

                      });
};

TripsApp.getAllTrips = function( ){
  TripsApp.lastAction = TripsApp.getAllTrips;
  TripsApp.getRequest("/api/v1/trips",
                      function( data, status, request ){
                        TripsApp.renderAllTripsTemplate({trips: JSON.parse(request.responseText), title: "Manage Trips"});
                      },
                      function(){});
};

TripsApp.getMyTrips = function( ){
  TripsApp.lastAction = TripsApp.getMyTrips;
  TripsApp.getRequest("/api/v1/trips/?by_user_id=" + TripsApp.user.id,
                      function( data, status, request ){
                        TripsApp.renderAllTripsTemplate({trips: JSON.parse(request.responseText), title: "My Trips"});
                      },
                      function(){});
};

TripsApp.getMyFilteredTrips = function( ){
  TripsApp.lastAction = TripsApp.getMyTrips;
  TripsApp.getRequest("/api/v1/trips/" + TripsApp.getTripFilters(TripsApp.user.id) ,
                      function( data, status, request ){
                        TripsApp.renderTripsListTemplate({trips: JSON.parse(request.responseText), title: "My Trips"});
                      },
                      function(){});
};

TripsApp.getFilteredTrips = function( ){
  TripsApp.lastAction = TripsApp.getAllTrips;
  TripsApp.getRequest("/api/v1/trips/" + TripsApp.getTripFilters(),
                      function( data, status, request ){
                        TripsApp.renderTripsListTemplate({trips: JSON.parse(request.responseText), title: "Manage Trips"});
                      },
                      function(){});
};

TripsApp.getTravelPlan = function(){
  var date = new Date(), y = date.getFullYear(), m = date.getMonth();
  var startDate = new Date(y, m + 1, 1);
  var endDate = new Date(y, m + 2, 0);
  TripsApp.getRequest("/api/v1/trips/?by_user_id=" + TripsApp.user.id + "&by_start_date_be=" + startDate.toISOString().slice(0, 10) + "&by_start_date_le=" + endDate.toISOString().slice(0, 10),
                      function( data, status, request ){
                        TripsApp.renderTravelPlanTemplate({trips: JSON.parse(request.responseText), title: "Manage Trips"});
                      },
                      function(){});
}

TripsApp.getTrip = function( id ){
  TripsApp.getRequest("/api/v1/trips/" + id,
                      function( data, status, request ){
                        TripsApp.renderEditTripTemplate({trip: JSON.parse(request.responseText)});
                      },
                      function(){});
};

TripsApp.getTripFilters = function(user_id = ""){
  var filters = ""
  var destination = $("#destination").val();
  var startDate = $("#startDate").val();
  var endDate = $("#endDate").val();
  var comment = $("#comment").val();
  if (startDate != ""){
    filters += "?by_start_date="+startDate;
  }
  if (endDate != ""){
    if (filters == ""){
      filters += "?by_end_date="+endDate;
    } else {
      filters += "&by_end_date="+endDate;
    }
  }
  if (comment != ""){
    if (filters == ""){
      filters += "?by_comment="+comment;
    } else {
      filters += "&by_comment="+comment;
    }
  }
  if (destination != ""){
    if (filters == ""){
      filters += "?by_destination="+destination;
    } else {
      filters += "&by_destination="+destination;
    }
  }
  if (user_id != ""){
    if (filters == ""){
      filters += "?by_user_id="+user_id;
    } else {
      filters += "&by_user_id="+user_id;
    }
  }
  return filters;
};
