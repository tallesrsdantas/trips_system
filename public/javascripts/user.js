TripsApp.signIn = function(email, password){
  $.post( "/api/v1/auth/sign_in", {email: email, password: password })
    .success( function( data, status, request ) {
      TripsApp.setUserFromRequest( request );
      TripsApp.renderMainTemplate();
      $.smkAlert({
        text: 'Successfully logged in!',
        type: 'success'
      });
    }).fail( function( data, status, request ) {
      $.smkAlert({
        text: TripsApp.errorsFromData(data),
        type: 'danger'
      });
    });
};

TripsApp.signUp = function(name, email, password, passwordConfirmation){
  $.post( "/api/v1/auth", {name: name, email: email, password: password, password_confirmation: passwordConfirmation })
    .success( function( data, status, request ) {
      TripsApp.setUserFromRequest( request );
      TripsApp.patchRequest("/api/v1/users/" + TripsApp.user.id , {user: { name: name}}, function( data, status, request ){
        TripsApp.setUserFromUpdate( request );
        TripsApp.renderMainTemplate();
        $.smkAlert({
          text: 'Welcome to TripsApp!',
          type: 'success'
        });
      }, function(){});
    }).fail( function( data, status, request ) {
      $.smkAlert({
        text: TripsApp.errorsFromData(data),
        type: 'danger'
      });
    });
};


TripsApp.signOut = function( ){
  TripsApp.deleteRequest("/api/v1/auth/sign_out",
                      function( data, status, request ){
                        TripsApp.cleanUserInformation();
                        TripsApp.renderSignInTemplate();
                        $.smkAlert({
                          text: 'Successfully logged out!',
                          type: 'success'
                        });
                      },
                      function(){
                        TripsApp.cleanUserInformation();
                        TripsApp.renderSignInTemplate();
                      });
};

TripsApp.validateToken = function( ){
  TripsApp.getRequest("/api/v1/auth/validate_token",
                      function( data, status, request ){
                        TripsApp.renderMainTemplate();
                      },
                      function(){
                        TripsApp.renderSignInTemplate();
                      });
};

TripsApp.updateUser = function( id, name, role ){
  TripsApp.patchRequest("/api/v1/users/" + id,
                       {user: { name: name, role: role }},
                      function( data, status, request ){
                        if (JSON.parse(request.responseText).id == TripsApp.user.id){
                          TripsApp.setUserFromUpdate(request);
                          TripsApp.renderMainTemplate();
                          TripsApp.lastAction();
                        } else {
                          TripsApp.getAllUsers();
                        }
                        $.smkAlert({
                          text: 'Successfully updated user!',
                          type: 'success'
                        });
                      },
                      function(){});
};

TripsApp.createUser = function( ){

};

TripsApp.deleteUser = function( id ){
  TripsApp.deleteRequest("/api/v1/users/" + id,
                      function( data, status, request ){
                        if(id == TripsApp.user.id){
                          TripsApp.validateToken();
                        } else {
                          TripsApp.lastAction();
                        }
                        $.smkAlert({
                          text: 'Successfully deleted out!',
                          type: 'success'
                        });
                      },
                      function(){

                      });
};

TripsApp.getAllUsers = function(  ){
  TripsApp.lastAction = TripsApp.getAllUsers;
  TripsApp.getRequest("/api/v1/users",
                      function( data, status, request ){
                        TripsApp.renderAllUsersTemplate({users: JSON.parse(request.responseText)});
                      },
                      function(){});
};

TripsApp.getUser = function( id ){
  TripsApp.getRequest("/api/v1/users/" + id,
                      function( data, status, request ){
                        TripsApp.renderEditUserTemplate({user: JSON.parse(request.responseText)});
                      },
                      function(){});
};

TripsApp.setUserFromRequest = function( request ){
  TripsApp.api.uid = request.getResponseHeader("Uid");
  TripsApp.api.accessToken = request.getResponseHeader("Access-Token");
  TripsApp.api.client = request.getResponseHeader("Client");
  TripsApp.api.expiry = request.getResponseHeader("Expiry");
  user = JSON.parse(request.responseText).data;
  TripsApp.user.name = user.name;
  TripsApp.user.email = user.email;
  TripsApp.user.role = user.role;
  TripsApp.user.id = user.id;
  TripsApp.setCookies();
};

TripsApp.setUserFromUpdate = function(request){
  user = JSON.parse(request.responseText);
  TripsApp.user.name = user.name;
  TripsApp.user.email = user.email;
  TripsApp.user.role = user.role;
  TripsApp.user.id = user.id;
  TripsApp.setCookies();
}

TripsApp.setCookies = function( request ){
  $.cookie("Access-Token", TripsApp.api.accessToken);
  $.cookie("Client", TripsApp.api.client);
  $.cookie("Uid", TripsApp.api.uid);
  $.cookie("Expiry", TripsApp.api.expiry);
  $.cookie("name", TripsApp.user.name);
  $.cookie("email", TripsApp.user.email);
  $.cookie("role", TripsApp.user.role);
  $.cookie("id", TripsApp.user.id);
};

TripsApp.cleanUserInformation = function( request ){
  TripsApp.api.uid = undefined;
  TripsApp.api.accessToken = undefined;
  TripsApp.api.client = undefined;
  TripsApp.api.expiry = undefined;
  TripsApp.user.name = undefined;
  TripsApp.user.email = undefined;
  TripsApp.user.role = undefined;
  TripsApp.user.id = undefined;
  $.cookie("Access-Token", "");
  $.cookie("Client", "");
  $.cookie("Uid", "");
  $.cookie("Expiry", "");
  $.cookie("name", "");
  $.cookie("email", "");
  $.cookie("role", "");
  $.cookie("id", "");
};
