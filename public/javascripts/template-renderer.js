TripsApp.renderTemplate = function(templateName, parameters, element, callback){
  parameters.currentUser = TripsApp.user
  var templateScript = ""
  $.get("/templates/" + templateName + ".hbs" , function(data) {
    templateScript = data;
    var template = Handlebars.compile (templateScript);
    element.html (template (parameters));
    var animationEnd = 'webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend';
    element.addClass('animated fadeIn').one(animationEnd, function() {
      $(this).removeClass('animated fadeIn');
    });
    $('.datepicker').datepicker({
        format: 'yyyy-mm-dd'
    });
    callback();
  });
};

TripsApp.renderSignInTemplate = function(parameters = {}){
  TripsApp.renderTemplate("sign-in", {}, $("#content"), function(){
    $("#sign-up-btn").click(function(){
      TripsApp.renderSignUpTemplate();
    });
    $("form").submit(function(){
      TripsApp.signIn($("#email").val(), $("#password").val());
      return false;
    });
  });
};

TripsApp.renderSignUpTemplate = function(parameters = {}){
  TripsApp.renderTemplate("sign-up", {}, $("#content"), function(){
    $("#sign-in-btn").click(function(){
      TripsApp.renderSignInTemplate();
    });
    $("form").submit(function(){
      TripsApp.signUp($("#name").val(), $("#email").val(), $("#password").val(), $("#password-confirmation").val());
      return false;
    });
  });
};

TripsApp.renderMainTemplate = function(parameters = {}){
  TripsApp.lastAction = TripsApp.getMyTrips;
  TripsApp.renderTemplate("main", {user: TripsApp.user}, $("#content"), function(){
    TripsApp.getMyTrips();
    $(".navbar-brand").click(function(){
      TripsApp.getMyTrips();
    });
    $("#manage-my-trips-link").click(function(){
      TripsApp.getMyTrips();
    });
    $("#manage-users-link").click(function(){
      TripsApp.getAllUsers();
    });
    $("#manage-trips-link").click(function(){
      TripsApp.getAllTrips();
    });
    $("#account-link").click(function(){
      TripsApp.lastAction = TripsApp.getMyTrips;
      TripsApp.getUser(TripsApp.user.id);
    });
    $("#sign-out-link").click(function(){
      TripsApp.signOut();
    });
  });
};

TripsApp.renderAllUsersTemplate = function(parameters = {}){
  TripsApp.renderTemplate("all-users", parameters, $("#info"), function(){
    $(".edit-btn").click(function(){
      TripsApp.getUser($(this).data("id"));
    });
    $(".delete-btn").click(function(){
      TripsApp.deleteUser($(this).data("id"));
    });
  });
};

TripsApp.renderEditUserTemplate = function(parameters = {}){
  TripsApp.renderTemplate("edit-user", parameters, $("#info"), function(){
    $("#back-btn").on("click", function(){
      TripsApp.lastAction();
    });
    $("form").submit(function(){
      TripsApp.updateUser($("#id").val(), $("#name").val(), $("#role").val());
      return false;
    });
  });
};

TripsApp.renderAllTripsTemplate = function(parameters = {}){
  TripsApp.renderTemplate("all-trips", parameters, $("#info"), function(){
    $("form").submit(function(){
      if (parameters.title == "My Trips"){
        TripsApp.getMyFilteredTrips();
      } else {
        TripsApp.getFilteredTrips();
      }
      return false;
    });
    $(".edit-btn").click(function(){
      TripsApp.getTrip($(this).data("id"));
    });
    $("#travel-plan-btn").click(function(){
      TripsApp.getTravelPlan();
    });
    $(".new-btn").click(function(){
      TripsApp.renderNewTripTemplate();
    });
    $(".delete-btn").click(function(){
      TripsApp.deleteTrip($(this).data("id"));
    });
  });
};

TripsApp.renderTravelPlanTemplate = function(parameters = {}){
  TripsApp.renderTemplate("travel-plan", parameters, $("#info"), function(){
  });
};

TripsApp.renderTripsListTemplate = function(parameters = {}){
  TripsApp.renderTemplate("trips-list", parameters, $("#trips"), function(){
    $(".edit-btn").click(function(){
      TripsApp.getTrip($(this).data("id"));
    });
    $(".delete-btn").click(function(){
      TripsApp.deleteTrip($(this).data("id"));
    });
  });
};

TripsApp.renderEditTripTemplate = function(parameters = {}){
  TripsApp.renderTemplate("edit-trip", parameters, $("#info"), function(){
    $("#back-btn").on("click", function(){
      TripsApp.lastAction();
    });
    $("form").submit(function(){
      TripsApp.updateTrip($("#id").val(), $("#destination").val(), $("#startDate").val(), $("#endDate").val(), $("#comment").val(), $("#userId").val());
      return false;
    });
  });
};

TripsApp.renderNewTripTemplate = function(parameters = {}){
  TripsApp.renderTemplate("new-trip", parameters, $("#info"), function(){
    $("#back-btn").on("click", function(){
      TripsApp.lastAction();
    });
    $("form").submit(function(){
      TripsApp.createTrip($("#destination").val(), $("#startDate").val(), $("#endDate").val(), $("#comment").val(), $("#userId").val());
      return false;
    });
  });
};
