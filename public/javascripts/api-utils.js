TripsApp.apiHeaders = function(){
  return {
    "Uid": TripsApp.api.uid,
    "Access-Token": TripsApp.api.accessToken,
    "Client": TripsApp.api.client,
    "Expiry": TripsApp.api.expiry
  };
};

TripsApp.getRequest = function(path, successCallback, failCallback){
  TripsApp.apiRequest("get", path, {}, successCallback, failCallback);
};

TripsApp.postRequest = function(path, data, successCallback, failCallback){
  TripsApp.apiRequest("post", path, data, successCallback, failCallback);
};

TripsApp.patchRequest = function(path, data, successCallback, failCallback){
  TripsApp.apiRequest("patch", path, data, successCallback, failCallback);
};

TripsApp.deleteRequest = function(path, successCallback, failCallback){
  TripsApp.apiRequest("delete", path, {}, successCallback, failCallback);
};

TripsApp.apiRequest = function(method, path, data, successCallback, failCallback){
  $.ajax({
    url: path,
    type: method,
    headers: TripsApp.apiHeaders(),
    data: data,
    dataType: "json",
    success: function( data, status, request ){
      successCallback( data, status, request );
    },
    error: function( data, status, request ){
      if (data.status == 401){
        if (path != "/api/v1/auth/validate_token") {
          $.smkAlert({
            text: "You are not allowed to perform this action!",
            type: 'danger'
          });
          TripsApp.validateToken();
        }
      } else if (path != "/api/v1/auth/validate_token") {
        $.smkAlert({
          text: TripsApp.errorsFromData(data),
          type: 'danger'
        });
      }
      failCallback( data, status, request );
    }
  });
};


TripsApp.errorsFromData = function( data ){
  var string = "";
  var errors;
  if (JSON.parse(data.responseText).errors == undefined){
    var errors = JSON.parse(data.responseText);
  } else if (JSON.parse(data.responseText).errors.full_messages != undefined){
    var errors = JSON.parse(data.responseText).errors.full_messages.getUnique();
  } else {
    var errors = JSON.parse(data.responseText).errors;
  }
  for(i=0;i<errors.length; i++){
    string += "<p>" + errors[i] + "</p>";
  }
  return string;
};
