Rails.application.routes.draw do
  mount_devise_token_auth_for 'User', at: 'api/v1/auth'
  namespace :api ,defaults: {format: :json} do
    namespace :v1 do
      resources :trips, only: [:update, :destroy, :create, :show, :index]
      resources :users, only: [:update, :destroy, :create, :show, :index]
    end
  end
end
