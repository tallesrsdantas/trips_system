class Trip < ActiveRecord::Base
  scope :by_user_id, -> user_id { where(user_id: user_id) }
  scope :by_destination, -> destination { where("destination like ?", "%" + destination + "%") }
  scope :by_comment, -> comment { where("comment like ?", "%" + comment + "%") }
  scope :by_start_date, -> start_date { where("start_date >= ?", start_date) }
  scope :by_start_date_be, -> start_date { where("start_date >= ?", start_date) }
  scope :by_start_date_le, -> start_date { where("start_date <= ?", start_date) }
  scope :by_end_date, -> end_date { where("end_date <= ?", end_date) }
  belongs_to :user
  validate :end_date_bigger_than_start_date
  validates_presence_of :destination, :start_date, :end_date, :user_id

  def as_json(options = { })
    h = super(options)
    h[:days]   = missing_days
    h[:user_name]   = user.try(:name)
    h
  end

  def missing_days
    return -1 if start_date <= Date.today
    (start_date - Date.today).to_i
  end

  def end_date_bigger_than_start_date
    return if end_date.nil? || start_date.nil?
    if end_date < start_date
      errors.add(:base, 'Invalid period.')
    end
  end
end
