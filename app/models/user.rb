class User < ActiveRecord::Base
  devise :database_authenticatable, :registerable,
          :recoverable, :rememberable, :validatable,
          :omniauthable
  has_many :trips, dependent: :delete_all
  validates_confirmation_of :password
  validates_presence_of :name, :email, :role
  include DeviseTokenAuth::Concerns::User

  def regular?
    role == 'regular'
  end

  def user_manager?
    role == 'user_manager'
  end

  def admin?
    role == 'admin'
  end
end
