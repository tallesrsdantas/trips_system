class Api::V1::UsersController < ApplicationController
  before_action :authenticate_user!
  before_filter :authorized_action?, only: [:update, :destroy, :show]
  rescue_from ActiveRecord::RecordNotFound, :with => :record_not_found

  def index
    unless current_user.regular?
      render json: User.all, status: :ok
    else
      render json: {}, status: :unauthorized
    end
  end

  def create
    @user = User.new(user_params)
    if @user.save
      render json: @user, status: :ok
    else
      render json: @user.errors.full_messages, status: :bad_request
    end
  end

  def update
    @user = User.find(params[:id])
    if @user.update(user_params)
      render json: @user, status: :ok
    else
      render json: @user.errors.full_messages, status: :bad_request
    end
  end

  def destroy
    @user = User.find(params[:id])
    if @user.destroy
      render json: {}, status: :ok
    else
      render json: @user.errors.full_messages, status: :bad_request
    end
  end

  def show
    @user = User.find(params[:id])
    render json: @user, status: :ok
  end

  private

  def record_not_found
    render json: {}, status: :not_found
  end

  def user_params
    if current_user.admin? || current_user.user_manager?
      params.require(:user).permit(:name, :email, :password, :password_confirmation, :role)
    else
      params.require(:user).permit(:name, :email, :password, :password_confirmation)
    end
  end

  def authorized_action?
    return true if current_user.admin? || current_user.user_manager? || User.find(params[:id]) == current_user
    render json: {}, status: :unauthorized
    return false
  end
end
