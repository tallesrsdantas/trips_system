class Api::V1::TripsController < ApplicationController
  has_scope :by_user_id
  has_scope :by_destination
  has_scope :by_comment
  has_scope :by_start_date
  has_scope :by_end_date
  has_scope :by_start_date_be
  has_scope :by_start_date_le
  before_action :authenticate_user!
  before_filter :authorized_action?, only: [:update, :destroy, :show]
  rescue_from ActiveRecord::RecordNotFound, :with => :record_not_found

  def index
    if current_user.admin? ||
       (!params[:by_user_id].nil? && params[:by_user_id] == current_user.id.to_s)
      render json: apply_scopes(Trip).all, status: :ok
    else
      render json: {}, status: :unauthorized
    end
  end

  def create
    if current_user.admin? || (!trip_params[:user_id].nil? && trip_params[:user_id] == current_user.id.to_s)
      @trip = Trip.new(trip_params)
      if @trip.save
        render json: @trip, status: :ok
      else
        render json: @trip.errors.full_messages, status: :bad_request
      end
    else
      render json: {}, status: :unauthorized
    end
  end

  def update
    @trip = Trip.find(params[:id])
    if @trip.update(trip_params)
      render json: @trip, status: :ok
    else
      render json: @trip.errors.full_messages, status: :bad_request
    end
  end

  def destroy
    @trip = Trip.find(params[:id])
    if @trip.destroy
      render json: {}, status: :ok
    else
      render json: @trip.errors.full_messages, status: :bad_request
    end
  end

  def show
    @trip = Trip.find(params[:id])
    render json: @trip, status: :ok
  end

  private

  def record_not_found
    render json: {}, status: :not_found
  end

  def trip_params
    params.require(:trip).permit(:destination, :start_date, :end_date, :comment, :user_id)
  end

  def authorized_action?
    return true if current_user.admin? || Trip.find(params[:id]).user.id == current_user.id
    render json: {}, status: :unauthorized
    false
  end
end
