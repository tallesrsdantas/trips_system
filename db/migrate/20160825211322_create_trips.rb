class CreateTrips < ActiveRecord::Migration
  def change
    create_table :trips do |t|
      t.string :destination
      t.date :start_date
      t.date :end_date
      t.string :comment
      t.integer :user_id

      t.timestamps null: false
    end
  end
end
